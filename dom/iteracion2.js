
/* 2.1 Inserta dinamicamente en un html un div vacio con javascript.
 */
const $$divVacio = document.createElement("div");
$$divVacio.className = 'vacio'
document.body.appendChild($$divVacio);

/* 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
 */
const $$div2 = document.createElement("div");
const $$p = document.createElement("p");
/* $$p.innerHTML = `
    <div>
        <h1>${nombre}</h1> 
        <p>hola</p>
    </div>
` */
$$div2.appendChild($$p);
 document.body.appendChild($$div2);

/* 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
 */
const $$div3 = document.createElement("div");

for(let i = 0; i < 6; i++){
   const $$p = document.createElement("p");
   $$div3.appendChild($$p)
}
document.body.appendChild($$div3);


/* 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
 */

const $$pSoyDinamico = document.createElement("p");
$$pSoyDinamico.textContent = "Soy Dinamico!";
document.body.appendChild($$pSoyDinamico);

/* 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
 */
const $$h2 = document.querySelector(".fn-insert-here");
$$h2.textContent = "Wubba Lubba dub dub";

/* 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
 */
const $$apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter']
const $$ul = document.createElement("ul");
for (let i = 0; i < $$apps.length; i++) {
    const el = $$apps[i];
    const $$li = document.createElement("li");
    $$li.innerText = el;
    $$ul.appendChild($$li);
}
document.body.appendChild($$ul);



/* 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
 */
const $$nodes =  document.querySelectorAll(".fn-remove-me");
for (let el of $$nodes){
    el.remove();
}


/* 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
	Recuerda que no solo puedes insertar elementos con .appendChild. */

const $$p7 = document.createElement("p");
$$p7.innerText = "Voy en medio!";
const $$divs = document.querySelectorAll("div");
document.body.insertBefore($$p7, $$divs[1]);
/* 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here */


const $$divs9 = document.querySelectorAll(".fn-insert-here");

for (const div of $$divs9) {
    let $$p9 = document.createElement("p");
    $$p9.textContent = "Voy dentro!";
    div.appendChild($$p9)
}
















